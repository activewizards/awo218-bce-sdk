## Requirements

- python 3.5.2

## 1. Install requirements: ( A or B )
    A.
        1.1. git clone https://bitbucket.org/activewizards/awo218-bce-sdk.git

        1.2. cd ~/awo218-bce-sdk

        1.3. python3 setup.py install    

    B.
        1. pip install git+https://bitbucket.org/activewizards/awo218-bce-sdk.git
